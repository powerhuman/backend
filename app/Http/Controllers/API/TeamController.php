<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\Team\CreateTeamRequest;
use App\Http\Requests\Team\UpdateTeamRequest;
use App\Models\Team;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Negartarh\APIWrapper\Facades\APIResponse;

class TeamController extends Controller
{
	public function fetch(Request $request)
	{
		$id = $request->input('id');
		$name = $request->input('name');
		$limit = $request->input('limit', 10);

		$teamQuery = Team::withCount('employees');

		if ($id) {
			$team = $teamQuery->find($id);

			if ($team) {
				return APIResponse::success($team, 'Team found');
			}

			return APIResponse::status(404, 'Team not found');
		}

		$teams = $teamQuery->where('company_id', $request->company_id);

		if ($name) {
			$teams->where('name', 'like', "%$name%");
		}

		return APIResponse::success($teams->paginate($limit), 'Teams found');
	}

	public function create(CreateTeamRequest $request)
	{
		try {
			/* Upload logo */
			if ($request->hasFile('icon')) {
				$path = $request->file('icon')->store('public/icons');
			}

			/* Create team */
			$team = Team::create([
				'name' => $request->name,
				'icon' => isset($path) ? $path : '',
				'company_id' => $request->company_id,
			]);

			if (!$team) {
				throw new Exception('Team not created');
			}

			return APIResponse::success($team, 'Team created');
		} catch (Exception $e) {
			return APIResponse::status(500, $e->getMessage());
		}
	}

	public function update(UpdateTeamRequest $request, $id)
	{
		try {
			/* Get team */
			$team = Team::find($id);

			/* Check if team exists */
			if (!$team) {
				throw new Exception('Team not found');
			}

			/* Upload icon */
			if ($request->hasFile('icon')) {
				$path = $request->file('icon')->store('public/icons');
			}

			/* Update team */
			$team->update([
				'name' => $request->name,
				'icon' => isset($path) ? $path : $team->icon, /* check if icon is set */
				'company_id' => $request->company_id,
			]);

			return APIResponse::success($team, 'Team updated');
		} catch (Exception $e) {
			return APIResponse::status(500, $e->getMessage());
		}
	}

	public function destroy($id)
	{
		try {
			// Get team
			$team = Team::find($id);

			// TODO: Check if team is owned by user

			// Check if team exists
			if (!$team) {
				throw new Exception('Team not found');
			}

			// Delete team
			$team->delete();

			return APIResponse::success('Team deleted');
		} catch (Exception $e) {
			return APIResponse::status(500, $e->getMessage());
		}
	}
}
