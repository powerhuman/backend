<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\Role\CreateRoleRequest;
use App\Http\Requests\Role\UpdateRoleRequest;
use App\Models\Role;
use Exception;
use Illuminate\Http\Request;
use Negartarh\APIWrapper\Facades\APIResponse;

class RoleController extends Controller
{
	public function fetch(Request $request)
	{
		$id = $request->input('id');
		$name = $request->input('name');
		$limit = $request->input('limit', 10);
		$with_responsibilities = $request->input('with_responsibilities', false);

		$roleQuery = Role::withCount('employees');

		if ($id) {
			$role = $roleQuery->with('responsibilities')->find($id);

			if ($role) {
				return APIResponse::success($role, 'Role found');
			}

			return APIResponse::status(404, 'Role not found');
		}

		$roles = $roleQuery->where('company_id', $request->company_id);

		if ($name) {
			$roles->where('name', 'like', "%$name%");
		}

		if ($with_responsibilities) {
			$roles->with('responsibilities');
		}

		return APIResponse::success($roles->paginate($limit), 'Roles found');
	}

	public function create(CreateRoleRequest $request)
	{
		try {
			/* Create role */
			$role = Role::create([
				'name' => $request->name,
				'company_id' => $request->company_id,
			]);

			if (!$role) {
				throw new Exception('Role not created');
			}

			return APIResponse::success($role, 'Role created');
		} catch (Exception $e) {
			return APIResponse::status(500, $e->getMessage());
		}
	}

	public function update(UpdateRoleRequest $request, $id)
	{
		try {
			/* Get role */
			$role = Role::find($id);

			/* Check if role exists */
			if (!$role) {
				throw new Exception('Role not found');
			}

			/* Update role */
			$role->update([
				'name' => $request->name,
				'company_id' => $request->company_id,
			]);

			return APIResponse::success($role, 'Role updated');
		} catch (Exception $e) {
			return APIResponse::status(500, $e->getMessage());
		}
	}

	public function destroy($id)
	{
		try {
			// Get role
			$role = Role::find($id);

			// TODO: Check if role is owned by user

			// Check if role exists
			if (!$role) {
				throw new Exception('Role not found');
			}

			// Delete role
			$role->delete();

			return APIResponse::success('Role deleted');
		} catch (Exception $e) {
			return APIResponse::status(500, $e->getMessage());
		}
	}
}
