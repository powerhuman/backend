<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\Company\CreateCompanyRequest;
use App\Http\Requests\Company\UpdateCompanyRequest;
use App\Models\Company;
use App\Models\User;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Negartarh\APIWrapper\Facades\APIResponse;

class CompanyController extends Controller
{
	/**
	 * get all company data with/without filter
	 *
	 * @param  mixed $request
	 * @return void
	 */
	public function fetch(Request $request)
	{
		$id = $request->input('id');
		$name = $request->input('name');
		$limit = $request->input('limit', 10);

		$companyQuery = Company::with('users')->whereHas('users', function ($query) {
			$query->where('user_id', Auth::id());
		});

		// endpoint:  powerhuman/api/company?id=1
		// Get single data
		if ($id) {
			$company = $companyQuery->find($id);

			if ($company) {
				return APIResponse::success($company);
			}

			return APIResponse::status(404, 'Company not found');
		}

		// endpoint: powerhuman/api/company
		// Get multiple data
		$companies = $companyQuery;

		// endpoiint: powerhuman/api/company?name=Lorem
		if ($name) {
			$companies->where('name', 'like', "%$name%");
		}

		return APIResponse::success($companies->paginate($limit), 'Companies found');
	}

	public function create(CreateCompanyRequest $request)
	{
		try {
			/* Upload logo */
			if ($request->hasFile('logo')) {
				$path = $request->file('logo')->store('public/logos');
			}

			/* Create company */
			$company = Company::create([
				'name' => $request->name,
				'logo' => isset($path) ? $path : '',
			]);

			if (!$company) {
				throw new Exception('Company not created');
			}

			/* Attach company to user */
			$user = User::find(Auth::id());
			$user->companies()->attach($company->id);

			/* Load users at company */
			$company->load('users');

			return APIResponse::success($company, 'Company created');
		} catch (Exception $e) {
			return APIResponse::status(500, $e->getMessage());
		}
	}

	public function update(UpdateCompanyRequest $request, $id)
	{
		try {
			/* Get company */
			$company = Company::find($id);

			/* Check if company exists */
			if (!$company) {
				throw new Exception('Company not found');
			}

			/* Upload logo */
			if ($request->hasFile('logo')) {
				$path = $request->file('logo')->store('public/logos');
			}

			/* Update company */
			$company->update([
				'name' => $request->name,
				'logo' => isset($path) ? $path : $company->logo,
			]);

			return APIResponse::success($company, 'Company updated');
		} catch (Exception $e) {
			return APIResponse::status(500, $e->getMessage());
		}
	}
}
