<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\Responsibility\CreateResponsibilityRequest;
use App\Models\Responsibility;
use Exception;
use Illuminate\Http\Request;
use Negartarh\APIWrapper\Facades\APIResponse;

class ResponsibilityController extends Controller
{
	public function fetch(Request $request)
	{
		$id = $request->input('id');
		$name = $request->input('name');
		$limit = $request->input('limit', 10);

		$responsibilityQuery = Responsibility::query();

		/* Get single data */
		if ($id) {
			$responsibility = $responsibilityQuery->find($id);

			if ($responsibility) {
				return APIResponse::success($responsibility, 'Responsibility found');
			}

			return APIResponse::status(404, 'Responsibility not found');
		}

		/* Get multiple data */
		$responsibilities = $responsibilityQuery->where('role_id', $request->role_id);

		if ($name) {
			$responsibilities->where('name', 'like', "%$name%");
		}

		return APIResponse::success($responsibilities->paginate($limit), 'Responsibilities found');
	}

	public function create(CreateResponsibilityRequest $request)
	{
		try {
			/* Create responsibility */
			$responsibility = Responsibility::create([
				'name' => $request->name,
				'role_id' => $request->role_id,
			]);

			if (!$responsibility) {
				throw new Exception('Responsibility not created');
			}

			return APIResponse::success($responsibility, 'Responsibility created');
		} catch (Exception $e) {
			return APIResponse::status(500, $e->getMessage());
		}
	}

	public function destroy($id)
	{
		try {
			// Get responsibility
			$responsibility = Responsibility::find($id);

			// TODO: Check if responsibility is owned by user

			// Check if responsibility exists
			if (!$responsibility) {
				throw new Exception('Responsibility not found');
			}

			// Delete responsibility
			$responsibility->delete();

			return APIResponse::success('Responsibility deleted');
		} catch (Exception $e) {
			return APIResponse::status(500, $e->getMessage());
		}
	}
}
