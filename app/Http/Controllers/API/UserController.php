<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\User;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Negartarh\APIWrapper\Facades\APIResponse;
use Laravel\Fortify\Rules\Password;

class UserController extends Controller
{
	/**
	 * login
	 *
	 * @param  mixed $request
	 * @return void
	 */
	public function login(Request $request)
	{
		try {
			// Validate request
			$request->validate([
				'email' => 'required|email',
				'password' => 'required',
			]);

			// Find user by email
			$credentials = request(['email', 'password']);
			if (!Auth::attempt($credentials)) {
				return APIResponse::unauthorized();
			}

			$user = User::where('email', $request->email)->first();
			if (!Hash::check($request->password, $user->password)) {
				throw new Exception('Invalid password');
			}

			// Generate token
			$tokenResult = $user->createToken('authToken')->plainTextToken;

			// Return response
			return APIResponse::success([
				'access_token' => $tokenResult,
				'token_type' => 'Bearer',
				'user' => $user
			], 'Login success');
		} catch (Exception $e) {
			return APIResponse::status(400, $e->getMessage(), 'Authentication Failed');
		}
	}

	public function register(Request $request)
	{
		try {
			// Validate request
			$request->validate([
				'name' => ['required', 'string', 'max:255'],
				'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
				'password' => ['required', 'string', new Password],
			]);

			// Create user
			$user = User::create([
				'name' => $request->name,
				'email' => $request->email,
				'password' => Hash::make($request->password),
			]);

			// Generate token
			$tokenResult = $user->createToken('authToken')->plainTextToken;

			// Return response
			return APIResponse::success([
				'access_token' => $tokenResult,
				'token_type' => 'Bearer',
				'user' => $user,
			], 'Register success');
		} catch (Exception $e) {
			// Return error response
			return APIResponse::status(400, $e->getMessage());
		}
	}

	/**
	 * logout
	 *
	 * @param  mixed $request
	 * @return void
	 */
	public function logout(Request $request)
	{
		// Revoke Token
		$token = $request->user()->currentAccessToken()->delete();

		// Return response
		return APIResponse::success($token, 'Logout success');
	}

	public function fetch(Request $request)
	{
		// Get user
		$user = $request->user();

		// Return response
		return APIResponse::success($user, 'Fetch success');
	}
}
