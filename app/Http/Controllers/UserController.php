<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class UserController extends Controller
{
  /**
   * index
   *
   * @param  mixed $request
   * @param  mixed $id
   * @return void
   */
  public function index(Request $request, $id)
  {
    return "This is " . $id;
  }
}
